// Настройки API
let api = "https://finance-app-wepro.herokuapp.com/"
let routes = {
    register: "auth/signup",
    login: "auth/login"
}

// Регистрация пользователя
let form = document.querySelector('form')

form.onsubmit = () => {
    event.preventDefault()

    let obj = {}
    let fm = new FormData(form)

    fm.forEach((value, key) => {
        obj[key] = value
    })

    axios.post(api + routes.register, obj)
        .then(res => {
            if (res.status == 200 || res.status == 201) {
                // Сохраняем экземпляр человека
                localStorage.user = JSON.stringify(res.data)
                // Кидаем его на главную страницу
                window.location.href = "./index3.html"

                // В дальнейшем будем писать JSON.parse(localStorage.user)

                return
            }

            throw new Error(":(")
        })
        .catch(err => {
            throw new Error(":(")
        })
}