let form = document.forms.newWallete;
let api = "https://finance-app-wepro.herokuapp.com/wallet/new"
let wallet = JSON.parse(localStorage.user);
let wallets = JSON.parse(localStorage.user).wallet || [] 

form.onsubmit = () => {
  event.preventDefault();

  let obj = {};
  let fm = new FormData(event.target);

  fm.forEach((value, key) => {
    obj[key] = value;
  });

  obj.user = +obj.user

  wallets.push(obj)

  console.log(obj);

  axios.post(api, obj)
    .then((res) => {
        console.log(res, '\n', obj);
        window.location.href = window.location.href.split('/')[3] = "wallets.html"
        localStorage.token = res.data.token 
        wallet.wallet = wallets
        localStorage.user = JSON.stringify(wallet)
    })
    .catch((err) => {
        console.log(err);
    });
};
