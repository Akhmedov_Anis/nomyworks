let form = document.forms.register;
let api = "https://finance-app-wepro.herokuapp.com/auth/signup"
let user = null;

form.onsubmit = () => {
  event.preventDefault();

  let obj = {};
  let fm = new FormData(event.target);

  fm.forEach((value, key) => {
    obj[key] = value;
  });

  axios.post(api, obj)
    .then((res) => {
        console.log(res, '\n', obj);
        window.location.href = window.location.href.split('/')[3] = "homepage.html"
        localStorage.token = res.data.token 
        localStorage.user = JSON.stringify(res.data)
        user = JSON.parse(localStorage.setItem('user'))
        console.log(user);
    })
    .catch((err) => {
        console.log(err);
    });
};
