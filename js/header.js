let user = null;

const check = () => {
  if (!localStorage.token) {
    window.location.href = window.location.href.split("/")[3] = "signIn.html";
    localStorage.clear();
  } else {
    console.log("Молодэц!");
    user = JSON.parse(localStorage.user);
  }
};
check();

let header = `<div class="left"><p><a href="./homepage.html">Главная</a></p><p><a href="./wallets.html">Мои кошельки</a></p><p><a href="./trans.html">Мои транзакции</a></p></div><div class="right"><p>${user.email}</p><svg class="logOut" width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.75 15.75H3.75C3.35218 15.75 2.97064 15.592 2.68934 15.3107C2.40804 15.0294 2.25 14.6478 2.25 14.25V3.75C2.25 3.35218 2.40804 2.97064 2.68934 2.68934C2.97064 2.40804 3.35218 2.25 3.75 2.25H6.75" stroke="#FF0000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M12 12.75L15.75 9L12 5.25" stroke="#FF0000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M15.75 9H6.75" stroke="#FF0000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></div>`;

let table = `<thead><tr><th>ID</th><th>Отправлено с помошью</th><th>Категория</th><th>Сумма транзакции</th><th>Когда</th></tr></thead> <tbody id="asd"></tbody>
`;

let mid = ` <main class="default_width"><div class="top another"><h1>Добро пожаловать</h1><p class="email">${user.email}</p></div></main>`;

let cards = document.getElementById("FlexBox");
let el = document.getElementById("header");
let mid_el = document.getElementById("main");
let table_el = document.getElementById("table");
let email = document.querySelector(".email");

el.innerHTML = header;
mid_el.innerHTML = mid;
table_el.innerHTML = table;
let tbody = document.querySelector("#asd");
console.log(tbody);

let LogOut = document.querySelector(".logOut");
let wallets = JSON.parse(localStorage.user).wallet;
let transactions = JSON.parse(localStorage.user).transation;

for (let item of wallets) {
  // console.log(item);
  let block = document.createElement("div");
  let p = document.createElement("p");
  let h1 = document.createElement("h1");

  block.classList.add("item");
  p.innerHTML = item.name;
  h1.innerHTML = item.currency;

  block.append(h1, p);
  cards.append(block);
}

for (let item of transactions) {
  console.log(item);
  let block = document.createElement("tr");
  let th_ID = document.createElement("th");
  let th_Methods = document.createElement("th");
  let th_category = document.createElement("th");
  let th_amount = document.createElement("th");
  let th_when = document.createElement("th");
  //   let th = document.createElement("th");

//   block.classList.add("item");

  th_ID.innerHTML = item.wallet;
  th_Methods.innerHTML = item.category;
  th_category.innerHTML = item.type;
  th_amount.innerHTML = item.ammount;
  th_when.innerHTML = new Date().getMinutes() + " минуты назад" ;

  block.append(th_ID, th_Methods, th_category, th_amount, th_when);
  tbody.append(block);
}

LogOut.onclick = () => {
  localStorage.clear();
  window.location.href = window.location.href.split("/")[3] = "signIn.html";
};
